const { WebClient } = require('@slack/client');
const path = require('path');
const fs = require('fs');
const token = process.env.GEESETEETHS;
const web = new WebClient(token);

// The Wild Goose Chase
var flock = fs.readdirSync('./geese');
var golden_goose = flock[Math.floor(Math.random() * flock.length)];

web.users.setPhoto({ image: fs.createReadStream("./geese/"+golden_goose) })
	.then((res) => { 
		console.log('Honk! Image now: ', golden_goose);
	})
	.catch(console.error);
